package ru.ncedu.showstool;

import java.util.Collection;

/**
 * Created by Никита on 03.07.14.
 */
public interface EpisodeGrabber {
    /**
     * Catches the episodes from the Json string
     * @param episodesJson - a string with the information about episodes
     * @return - collection ot the episodes from seasonNumber
     * @throws java.lang.IllegalArgumentException if the episodesJson is null
     *                                            if an error when reading the objects tree
     *                                            if Json parsing error
     */
    public Collection<Episode> getEpisodesOfSeason(String episodesJson);
}
