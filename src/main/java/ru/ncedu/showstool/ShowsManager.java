package ru.ncedu.showstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Никита on 24.07.14.
 */
public class ShowsManager {
    private static Logger logger = LoggerFactory.getLogger(ShowsManager.class);

    private static DataDownloader downloader = new DataDownloader();
    private static EpisodeGrabber grabber = new MyShowsEpisodeGrabber();


    private static EntityManagerFactory entityMf = Persistence.createEntityManagerFactory("showsPersistence");
    private static EntityManager entityManager = entityMf.createEntityManager();

    private static EntityManagerFactory getEntityMf() {
        if (entityMf == null || (!entityMf.isOpen())) {
            return (entityMf = Persistence.createEntityManagerFactory("showsPersistence"));
        }
        return entityMf;
    }

    private static EntityManager getEntityManager() {
        if (entityManager == null || (!entityManager.isOpen())) {
            return (entityManager = getEntityMf().createEntityManager());
        }
        return entityManager;
    }

    private List<Show> searchShows(String url) {
        return null;
    }

    private List<Episode> fetchEpisodes(String url) {
        logger.info("Start. url: " + url);
        String jsonResp = downloader.fetchJsonByUrl(url);
        List<Episode> episodes = (List<Episode>)grabber.getEpisodesOfSeason(jsonResp);
        logger.info("Finish. Size of episodes collection is: " + episodes.size());
        return episodes;
    }

    public void initializeDb() {
        logger.info("Start.");
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        logger.info("Transaction is active: " + entityManager.getTransaction().isActive());

        Show show = new Show("Game of Thrones", 2011L);
        entityManager.persist(show);
        logger.info("Show persisted: " + show.getId());

        List<Episode> episodes = fetchEpisodes("http://api.myshows.ru/shows/11945");
        for (Episode episode: episodes) {
            entityManager.persist(episode);
            logger.info("Episode persisted: " + episode.getId());
        }

        entityManager.getTransaction().commit();
    }

    public List<Show> getShows() {
        /////////////////////
        initializeDb();
        ////////////////////
        logger.info("Start.");

        EntityManager entityManager = getEntityManager();
        List<Show> shows = entityManager.createNativeQuery("select * from shows", Show.class).getResultList();

        logger.info("Finish. Selected shows count: " + shows.size());
        logger.debug("Finish. Selected shows:" + shows.toString());
        return shows;
    }

    public List<Episode> getEpisodes() {
        logger.info("Start.");
        EntityManager entityManager = getEntityManager();
        List<Episode> episodes = entityManager.createNativeQuery("select * from episodes", Episode.class).getResultList();
        logger.info("Finish. Selected episodes count: " + episodes.size());
        logger.debug("Finish. Selected episodes:" + episodes.toString());
        return episodes;
    }

    public List<Episode> getEpisodes(Long showId) {
        logger.info("Start. showId: " + showId);
        if (showId == null) {
            throw new IllegalArgumentException("showId can't be null");
        }
        EntityManager entityManager = getEntityManager();
        List<Episode> episodes = entityManager.createQuery(
                                 "select e from Episode e where e.show = :showId", Episode.class)
                                 .setParameter("showId", showId).getResultList();
        logger.info("Finish. Selected episodes count: " + episodes.size());
        logger.debug("Finish. Selected episodes:" + episodes.toString());
        return episodes;
    }
}

