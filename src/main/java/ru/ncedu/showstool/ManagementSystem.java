package ru.ncedu.showstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.*;

/**
 * Created by Али on 23.07.14.
 */
public class ManagementSystem {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("showsPersistence");
    private static EntityManager em = emf.createEntityManager();
    private static List<Episode> episodeList;
    private static List<Show> showList;

    private static Logger logger = LoggerFactory.getLogger(Episode.class);

    public ManagementSystem() {

    }

    /**
     * Initialize database by data from myshows
     */
    public static void initializeDB(){
        String jsonSourse = (new DataDownloader()).fetchJsonByUrl("http://api.myshows.ru/shows/11945");

        EpisodeGrabber grabber = new MyShowsEpisodeGrabber();

        episodeList = (List<Episode>)grabber.getEpisodesOfSeason(jsonSourse);

        Collections.sort(episodeList, new Comparator<Episode>() {
            @Override
            public int compare(Episode o1, Episode o2) {
                if (o1.getEpisodeNumber() < o2.getEpisodeNumber())
                    return -1;
                return 1;
            }
        });

        showList = new ArrayList<Show>();
        showList.add(new Show("GOT", 2011l));
        for (int i=0; i<episodeList.size(); i++)
            episodeList.get(i).setEpisodeShow(showList.get(0));

        showList.add(new Show("Scooby doo", 1965l));
        episodeList.add(new Episode("The horrors of the crocodile's lair", new Date(2000, 10, 10), 1l, 1l));
        episodeList.get(episodeList.size()-1).setEpisodeShow(showList.get(1));

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        for (Episode episode: episodeList)
            em.persist(episode);
        tx.commit();
    }

    /**
     * Find the episode in database
     * @param episodeID - episode's ID of show that will be found
     * @return found record
     * @throws java.lang.IllegalArgumentException - if episode was not found
     */
    public Episode findEpisode(Long episodeID)
    {
        logger.info("Finding episode with ID = " + episodeID);
        List<Episode> e = (List<Episode>)em.createQuery("SELECT e FROM Episode e WHERE e.id = :id").setParameter("id", episodeID).getResultList();
        //Episode e = em.find(Episode.class, episodeID);

        if (e.size() == 0)
            throw new IllegalArgumentException("There is not such record in database");

        return e.get(0);
    }

    /**
     * Add the episode in database
     * @param episode - episode of show that will be added
     * @param showID - ID of episode's show
     * @throws java.lang.IllegalArgumentException - if episode is null
     */
    public void addEpisode(Episode episode, Long showID)
    {
        if (episode == null)
            throw new IllegalArgumentException("Episode can not have a null value");

        logger.info("Adding episode in show with showID = " + showID);

        Show show = em.find(Show.class, showID);
        episode.setEpisodeShow(show);

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(episode);
        tx.commit();
        logger.info("Finished to add of episode with ID = " + episode.getId());
    }

    /**
     * Delete the episode in database
     * @param episodeID - episode's ID that will be deleted
     * @throws java.lang.IllegalArgumentException - if the record was not been deleted from the database
     */
    public void deleteEpisode(Long episodeID)
    {
        logger.info("Deleting episode with ID = " + episodeID);

        Episode episode = findEpisode(episodeID);
        //episodeList = getEpisodeList();

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        int queryRes = em.createQuery("DELETE FROM Episode e WHERE e.id = :id").setParameter("id", episodeID).executeUpdate();
        tx.commit();
        //episodeList = getEpisodeList();

        if (queryRes<=0)
            throw new IllegalStateException("Deleting a record from the database error");
    }

    /**
     * Change the episode in database
     * @param newEpisode - episode that will be changed
     * @throws java.lang.IllegalArgumentException - if there is not such record in database
     *                                              if episode is null
     */
    public void changeEpisode(Episode newEpisode)
    {
        if (newEpisode == null)
            throw new IllegalArgumentException("Episode must not have a null value");
        if (newEpisode.getId() == null && findEpisode(newEpisode.getId()) != null)
            throw new IllegalArgumentException("There is not such record in database");

        logger.info("Change episode with ID = " + newEpisode.getId());
        em.getTransaction().begin();
        em.merge(newEpisode);
        em.getTransaction().commit();
    }

    /**
     * Add the show in database
     * @param show - show that will be added
     * @throws java.lang.IllegalArgumentException - if show is null
     */
    public void addShow(Show show)
    {
        if (show == null)
            throw new IllegalArgumentException("Show can not have a null value");

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(show);
        tx.commit();
        logger.info("Show with ID = " + show.getId() + " was added");
    }


    /**
     * Delete the show in database
     * @param showID - ID of show that will be deleted
     * @throws java.lang.IllegalArgumentException - if the record was not been deleted from the database
     */
    public void deleteShow(Long showID)
    {
        logger.info("Delete show with ID = " + showID);
        Show show = findShow(showID);

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        int queryRes = em.createQuery("DELETE FROM Episode e WHERE e.episodeShow.id = :id").
                       setParameter("id", showID).executeUpdate() +
                       em.createQuery("DELETE FROM Show s WHERE s.id = :id").setParameter("id", showID).executeUpdate();
        tx.commit();

        if (queryRes<=0)
            throw new IllegalStateException("Deleting a record from the database error");
    }

    /**
     * Change the show in database
     * @param newShow - show that will be changed
     * @throws java.lang.IllegalArgumentException if show is null
     */
    public void changeShow(Show newShow)
    {
        if (newShow == null)
            throw new IllegalArgumentException("Show must not have a null value");
        if (newShow.getId() == null && findEpisode(newShow.getId()) != null)
            throw new IllegalArgumentException("There is not such record in database");

        logger.info("Change episode with ID = " + newShow.getId());
        em.getTransaction().begin();
        em.merge(newShow);
        em.getTransaction().commit();
    }

    /**
     * Find the show in database
     * @param showID - ID of show that will be found
     * @return found record
     * @throws java.lang.IllegalArgumentException - if show was not found
     */
    public Show findShow(Long showID)
    {
        logger.info("Finding show with ID = " + showID);
        List<Show> s = em.createQuery("SELECT s FROM Show s WHERE s.id = :id").setParameter("id", showID).getResultList();

        if (s.size() == 0)
            throw new IllegalArgumentException("There is not such record in database");

        return s.get(0);
    }

    /**
     * Get episode list from database
     * @return collection of episodes ordered by episode number
     */
    public List<Episode> getEpisodeList() {
        episodeList = em.createQuery("select e from Episode e order by e.id").getResultList();
        return episodeList;
    }

    public Long getCountEpisodesInShow(Long showID) {
        return (Long)em.createQuery("select count(e) from Episode e where e.episodeShow.id = :id").setParameter("id", showID).getSingleResult();
    }

    public List<Episode> getEpisodeListOfShow(Long showID) {
        List<Episode> resL = em.createQuery("select e from Episode e where e.episodeShow.id = :id order by e.seasonNumber, e.episodeNumber", Episode.class).
                setParameter("id", showID).getResultList();
        return resL;
    }

    /**
     * Get show list from database
     * @return collection of shows
     */
    public List<Show> getShowList() {
        showList = em.createQuery("select s from Show s").getResultList();
        return showList;
    }

}
