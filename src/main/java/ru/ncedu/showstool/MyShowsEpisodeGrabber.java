package ru.ncedu.showstool;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by Никита on 03.07.14.
 */
public class MyShowsEpisodeGrabber implements EpisodeGrabber {

    private static Logger logger = LoggerFactory.getLogger(EpisodeGrabber.class);
    /**
     * Catches the episodes from the Json string
     * @param episodesJson - a string with the information about episodes
     * @return - collection ot the episodes from seasonNumber
     * @throws java.lang.IllegalArgumentException if the episodesJson is null
     *                                            if an error when reading the objects tree
     *                                            if Json parsing error
     */
    @Override
    public Collection<Episode> getEpisodesOfSeason(String episodesJson) {
        logger.info("Starting. episodesJson (use debug level to show string)");
        logger.debug("episodesJson: " + episodesJson);

        if (episodesJson == null) {
            throw new IllegalArgumentException("JSON string for parsing can't be null");
        }

        List<Episode> result = new ArrayList<Episode>();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode episodesNode;

        try {
            JsonNode rootNode = objectMapper.readTree(episodesJson);
            episodesNode = rootNode.path("episodes");

            Map<String, Episode> resMap = objectMapper.readValue(episodesNode, HashMap.class);
            Episode episode;
            for (Map.Entry<String, Episode> entry: resMap.entrySet()) {
                episode = objectMapper.convertValue(entry.getValue(), Episode.class);
                result.add(episode);
            }
        } catch (JsonParseException e) {
            throw new IllegalArgumentException("Wrong JSON string argument: Json parsing error", e);
        } catch (IOException e) {
            throw new IllegalArgumentException("Wrong JSON string argument: an error when reading the objects tree", e);
        }

        if (result.size() < 2) {
            logger.info("Finish. Collection of episodes: " + result.toString());
        } else {
            logger.info("Finish. Collection of episodes is large (Use debug level)");
            logger.debug("Collection of episodes: " + result.toString());
        }
        return result;
    }
}
