package ru.ncedu.showstool;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Никита on 03.07.14.
 */
public class DataDownloader {
    private static Logger logger = LoggerFactory.getLogger(DataDownloader.class);

    /**
     * Downloads a Json string from the given url
     * @param url - url for downloading Json
     * @return - Json string
     * @throws java.lang.IllegalArgumentException if the url is null
     *                                            if Url is not accessible or bad reading
     * @throws java.lang.IllegalStateException if there is impossible to close BufferReader
     */
    public String fetchJsonByUrl(String url) {
        logger.info("Starting. url = " + url);
        if (url == null) {
            throw new IllegalArgumentException("URL can't be null");
        }
        HttpClient client = HttpClientBuilder.create().build();

        HttpGet request = new HttpGet(url);
        BufferedReader bufferReader = null;

        try {
            HttpResponse response = client.execute(request);
            bufferReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = bufferReader.readLine()) != null)
                result.append(line);

            if (result.length() < 10) {
                logger.info("Finish. Returning Json string: " + result.toString());
            } else {
                logger.info("Finish. Returning Json string is so long (Use debug level)");
                logger.debug("Returning Json string: "  + result.toString());
            }

            return result.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException("Url is not accessible or bad reading", e);
        } finally {
            request.completed();
            if (bufferReader != null)
                try {
                    bufferReader.close();
                } catch (IOException e) {
                    throw new IllegalStateException("There is impossible to close BufferReader", e);
                }
        }
    }
}
