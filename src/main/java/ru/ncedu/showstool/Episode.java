package ru.ncedu.showstool;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Никита on 02.07.14.
 */
@Entity
@Table(name = "episodes")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Episode{
    @Transient
    private static Logger logger = LoggerFactory.getLogger (Episode.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "episode_id")
    @JsonIgnore
    private Long id;

    @JsonProperty("title")
    private String title = "";

    @JsonProperty("airDate")
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date airDate;

    @JsonProperty("seasonNumber")
    private Long seasonNumber;

    @JsonProperty("episodeNumber")
    private Long episodeNumber;

//
    @ManyToOne (cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
    @JoinColumn(name="show_id" ,referencedColumnName = "show_id")
    private Show episodeShow;

    public Show getEpisodeShow() {
        return episodeShow;
    }

    public void setEpisodeShow(Show episodeShow) {
        this.episodeShow = episodeShow;
    }

    public Episode(){}

    public Episode(String title, Date airDate, Long seasonNumber, Long episodeNumber) {
        this.title = title;
        this.airDate = airDate;
        this.seasonNumber = seasonNumber;
        this.episodeNumber = episodeNumber;
    }

    /**
     * Compares two episodes
     * @param other
     * @return true if episodes are equals
     *         false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof Episode))
            return false;
        Episode episode = (Episode) other;
        if (/*episode.id.equals(id) &&*/
            episode.title.equals(((Episode) other).title) &&
            episode.airDate.equals(((Episode) other).airDate) &&
            episode.seasonNumber.equals(((Episode) other).seasonNumber) &&
            episode.episodeNumber.equals(((Episode) other).episodeNumber)) {
            return true;
        }
        return false;
    }



    /**
     * @return string representation of Episode object
     */
    @Override
    public String toString() {
        return "Episode{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", airDate='" + airDate + '\'' +
                ", seasonNumber=" + seasonNumber +
                ", episodeNumber=" + episodeNumber +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getAirDate() {
        return airDate;
    }

    public void setAirDate(Date airDate) {
        this.airDate = airDate;
    }

    public Long getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Long seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public Long getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Long episodeNumber) {
        this.episodeNumber = episodeNumber;
    }



}


