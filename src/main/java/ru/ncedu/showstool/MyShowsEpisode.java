package ru.ncedu.showstool;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Никита on 02.07.14.
 */

@JsonIgnoreProperties(ignoreUnknown=true)
public class MyShowsEpisode implements Episode{
    private Logger logger = LoggerFactory.getLogger (Episode.class);

    @JsonProperty("id")
    private Long id;
    @JsonProperty("title")
    private String title = "";
    @JsonProperty("airDate")
    private String airDate = "";
    @JsonProperty("seasonNumber")
    private Long seasonNumber;
    @JsonProperty("episodeNumber")
    private Long episodeNumber;

    public MyShowsEpisode(){}

    /**
     * Compares two episodes
     * @param other
     * @return true if episodes are equals
     *         false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof MyShowsEpisode))
            return false;
        MyShowsEpisode episode = (MyShowsEpisode) other;
        if (episode.id.equals(id) &&
            episode.title.equals(title) &&
            episode.airDate.equals(airDate) &&
            episode.seasonNumber.equals(seasonNumber) &&
            episode.episodeNumber.equals(episodeNumber)) {
            return true;
        }
        return false;
    }

    /**
     * @return string representation of MyShowsEpisode object
     */
    @Override
    public String toString() {
        return "Episode{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", airDate='" + airDate + '\'' +
                ", seasonNumber=" + seasonNumber +
                ", episodeNumber=" + episodeNumber +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Parses an air date from the class-internal string representation to the java.util.Date object
     * @return Episode's air date
     */
    @Override
    public Date getAirDate() {
        try {
            return new SimpleDateFormat("dd.MM.yyyy").parse(airDate);
        } catch (ParseException e) {
            logger.error("Bad parsing an air date from the string! (episode id = " + id + ")");
        }
        return new Date();
    }

    public void setAirDate(String airDate) {
        this.airDate = airDate;
    }

    @Override
    public Long getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Long seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    @Override
    public Long getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Long episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public MyShowsEpisode(Long id, String title, String airDate, Long seasonNumber, Long episodeNumber) {
        this.id = id;
        this.title = title;
        this.airDate = airDate;
        this.seasonNumber = seasonNumber;
        this.episodeNumber = episodeNumber;
    }
}


