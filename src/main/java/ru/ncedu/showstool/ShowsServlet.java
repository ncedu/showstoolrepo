package ru.ncedu.showstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Никита on 23.07.14.
 */
public class ShowsServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ShowsServlet.class);
    private static ManagementSystem ms = new ManagementSystem();

    /**
     * Converts parameters map Map<String, String[]> to Map<String, String>
     * @param arrayMap - parameters map
     * @return
     */
    public static Map<String, String> flatten(Map<String, String[]> arrayMap){
        Map<String, String> r = new HashMap<String, String>();
        for (Map.Entry<String, String[]> entry: arrayMap.entrySet()){
            String[] value = entry.getValue();
            if (value !=null && value.length > 0) r.put(entry.getKey(), value[0]);
        }
        return r;
    }

    /**
     * <p/>Called by the server (via the <code>service</code> method) to
     * allow a servlet to handle a GET request.<p/>
     * <p>Depends on the different request parameters implements next logic:</p>
     * request parameters:
     *     - title, year, id, mode - edits or adds new show using a managmentSystem
     *                               then outputs a shows list
     *     - show_id - outputs the episodes of the show(by show_id)
     *     - show_id, del_ep_id - deletes the episode of the show(show_id) by the del_ep_id and
     *                            outputs the episodes of the show(by the show_id)
     *     - no parameters - outputs a shows list
     * <p>If the request is incorrectly formatted, <code>doGet</code>
     * returns an HTTP "Bad Request" message.
     *
     * @param req  an {@link javax.servlet.http.HttpServletRequest} object that
     *             contains the request the client has made
     *             of the servlet
     * @param resp an {@link javax.servlet.http.HttpServletResponse} object that
     *             contains the response the servlet sends
     *             to the client
     * @throws java.io.IOException            if an input or output error is
     *                                        detected when the servlet handles
     *                                        the GET request
     * @throws javax.servlet.ServletException if the request for the GET
     *                                        could not be handled
     * @see javax.servlet.ServletResponse#setContentType
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Start.");
        Map<String, String> reqParam = flatten(req.getParameterMap());
        logger.info("Request parameters: " + reqParam);

        if (reqParam.containsKey("title") && reqParam.containsKey("year") &&
            reqParam.containsKey("id") && reqParam.containsKey("mode")) {

            logger.info("mode = " + reqParam.get("mode") + " show: " + reqParam);

            if (reqParam.get("mode").equals("add")) {
                ms.addShow(new Show(reqParam.get("title"), new Long(reqParam.get("year"))));
            } else {
                Show show = ms.findShow(new Long(reqParam.get("id")));
                show.setName(reqParam.get("title"));
                show.setYear(new Long(reqParam.get("year")));
                ms.changeShow(show);
            }
        }

        //ms.initializeDB();

        if (reqParam.containsKey("show_id") && reqParam.size() <= 2) {
            if (req.getParameterMap().containsKey("del_ep_id")) {
                ms.deleteEpisode(new Long(reqParam.get("del_ep_id")));
            }
            List<Episode> episodes = ms.getEpisodeListOfShow(new Long(reqParam.get("show_id")));
            logger.debug("episodes: " + episodes.toString());
            PrintWriter writer = resp.getWriter();
            resp.setContentType("text/html");
            writer.println("<html>\n" +
                           "    <head>\n" +
                           "        <meta charset=\"utf-8\">\n" +
                           "        <title>Episodes</title>\n" +
                           "    </head>\n" +
                           "    <body>\n" +
                           "        <form method=\"GET\" action=\"../shows\">" +
                           "            <input type=\"submit\" value=\"return\">" +
                           "        </form>" +
                           "        <table border=\"1\">\n" +
                           "            <caption>Episodes</caption>\n" +
                           "            <tr>\n" +
                           "                <th>&#8470</th>\n" +
                           "                <th>Title</th>\n" +
                           "                <th>AirDate</th>\n" +
                           "                <th>Season</th>\n" +
                           "                <th></th>\n" +
                           "            </tr>\n");

            for(Episode episode: episodes) {
                writer.println(
                           "            <tr>\n" +
                           "                <td>" + episode.getEpisodeNumber() + "</td>" +
                           "                <td>" + episode.getTitle() +"</td>" +
                           "                <td>" + (new SimpleDateFormat("dd-MM-yyyy").format(episode.getAirDate())) +"</td>" +
                           "                <td>" + episode.getSeasonNumber() +"</td>" +
                           "                <td>" +
                           "                    <form method=\"GET\" action=\"../shows\">" +
                           "                        <input type=\"submit\" value=\"Delete\">" +
                           "                        <input type=\"hidden\" name=\"show_id\" value=\"" + reqParam.get("show_id") + "\">" +
                           "                        <input type=\"hidden\" name=\"del_ep_id\" value=\"" + episode.getId() + "\">" +
                           "                    </form>" +
                           "                </td>" +
                           "            </tr>");
            }
            writer.println("        </table>\n" +
                           "    </body>\n" +
                           "</html>");
        } else {
            List<Show> shows = ms.getShowList();//manager.getShows();
            logger.debug("shows: " + shows.toString());
            PrintWriter writer = resp.getWriter();
            resp.setContentType("text/html");
            writer.println("<html>\n" +
                           "    <head>\n" +
                           "        <meta charset=\"utf-8\">\n" +
                           "        <title>Shows</title>\n" +
                           "    </head>\n" +
                           "    <body>\n" +
                           "        <form method=\"GET\" action=\"../editShow.jsp\">" +
                           "            <input type=\"hidden\" name=\"id\" value=\"\">" +
                           "            <input type=\"hidden\" name=\"title\" value=\"\">" +
                           "            <input type=\"hidden\" name=\"year\" value=\"\">" +
                           "            <input type=\"hidden\" name=\"mode\" value=\"add\">" +
                           "            <input type=\"submit\" value=\"add\">" +
                           "        </form>" +
                           "        <table border=\"1\">\n" +
                           "            <caption>Shows</caption>\n" +
                           "            <tr>\n" +
                           "                <th>Title</th>\n" +
                           "                <th>Year</th>\n" +
                           "                <th>Episodes</th>\n" +
                           "            </tr>\n");
            for (Show show: shows) {
                writer.println(
                           "            <tr>\n" +
                           "                <td><a href=\"../shows?show_id=" + show.getId() + "\">" + show.getName() + "</a></td>" +
                           "                <td>" + show.getYear() +"</td>" +
                           "                <td>" + ms.getCountEpisodesInShow(show.getId()) +"</td>" +
                           "                <td>" +
                           "                    <form method=\"GET\" action=\"../editShow.jsp\">" +
                           "                        <input type=\"hidden\" name=\"id\" value=\"" + show.getId() + "\">" +
                           "                        <input type=\"hidden\" name=\"title\" value=\"" + show.getName() + "\">" +
                           "                        <input type=\"hidden\" name=\"year\" value=\"" + show.getYear() + "\">" +
                           "                        <input type=\"hidden\" name=\"mode\" value=\"Change\">" +
                           "                        <input type=\"submit\" value=\"Change\">" +
                           "                    </form>" +
                           "                </td>" +
                           "            </tr>");
            }
            writer.println("        </table>\n" +
                           "    </body>\n" +
                           "</html>");
        }
    }
}
