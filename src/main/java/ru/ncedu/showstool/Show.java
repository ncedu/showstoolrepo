package ru.ncedu.showstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Али on 23.07.14.
 */
@Entity
@Table (name = "shows")
public class Show {
    @Transient
    private static Logger logger = LoggerFactory.getLogger(Show.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "show_id")
    private Long id;

    private String name;

    private Long year;

//    @OneToMany
//    @OneToMany(cascade = CascadeType.PERSIST)
//       @JoinTable(name = "episodes",
//        joinColumns = @JoinColumn(name = "shows_fk", referencedColumnName="show_id", nullable = false),
//        inverseJoinColumns = @JoinColumn(name = "episodes_fk", referencedColumnName="ID", nullable = false))
//    @OneToMany(cascade={CascadeType.ALL}, mappedBy = "episodeShow")
//    private List<Episode> episodeList = new ArrayList<Episode>();

    public Show() {}

    public Show(/*List<Episode> episodeList,*/ String name, Long year) {
        //this.episodeList = episodeList;
        this.name = name;
        this.year = year;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        Show.logger = logger;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }
//
//    public List<Episode> getEpisodeList() {
//        return episodeList;
//    }
//
//    public void setEpisodeList(List<Episode> episodeList) {
//        this.episodeList = episodeList;
//    }

    @Override
    public String toString() {
        return "Show{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Show show = (Show) o;

        if (name != null ? !name.equals(show.name) : show.name != null) return false;
        if (year != null ? !year.equals(show.year) : show.year != null) return false;

        return true;
    }
}
