package ru.ncedu.showstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Никита on 03.07.14.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Starting. main function args: " + args.toString());

        ShowsManager manager = new ShowsManager();
        //manager.initializeDb();
        //manager.getShows();
        //manager.getEpisodes();
        //manager.getEpisodes(1L);

//        try {
//            Long seasonNumber = 1L;
//            String jsonSourse = (new DataDownloader()).fetchJsonByUrl("http://api.myshows.ru/shows/11945");
//
//            logger.info("Getting a list of episodes of the " + seasonNumber.toString() + " season");
//
//            EpisodeGrabber grabber = new MyShowsEpisodeGrabber();
//            List<Episode> episodes = (List<Episode>) grabber.getEpisodesOfSeason(jsonSourse);
//
//            logger.info("Sorting the episodes list by the episode number");
//
//            Collections.sort(episodes, new Comparator<Episode>() {
//                @Override
//                public int compare(Episode o1, Episode o2) {
//                    if (o1.getEpisodeNumber() < o2.getEpisodeNumber())
//                        return -1;
//                    return 1;
//                }
//            });
//
//            System.out.printf("%10s %10s %40s %20s\n", "Season", "Episode", "Name", "Air Date");
//            for(Episode ep: episodes) {
//                System.out.printf("%10s %10s %40s %20s\n", ep.getSeasonNumber(), ep.getEpisodeNumber(), ep.getTitle(),
//                        new SimpleDateFormat("dd-MM-yyyy").format(ep.getAirDate()));
//            }
//            logger.info("Finish. Showed episodes: " + episodes.toString());
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
    }
}
