<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="java.util.Map" %>
<%--
  Created by IntelliJ IDEA.
  User: Никита
  Date: 25.07.14
  Time: 2:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Editing the episode</title>
</head>
<body>
    <%
        Logger logger = LoggerFactory.getLogger("editShow.jsp");
        Map<String, String> reqParams = ru.ncedu.showstool.ShowsServlet.flatten(request.getParameterMap());
        logger.info("Request parameters: " + reqParams);
    %>
<form name="edit" action="../shows" method="GET">
    Title: <input type="text" name="title" value="<%=reqParams.get("title")%>"><br>
    Year:  <input type="text" name="year" value="<%=reqParams.get("year")%>"><br>
    <input type="hidden" name="id" value="<%=reqParams.get("id")%>"><br>
    <input type="submit" name="mode" value="<%=reqParams.get("mode")%>">
</form>
</body>
</html>
