package ru.ncedu.showstool;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by Али on 25.07.14.
 */
public class ManagementSystemTest {
    private static ManagementSystem ms;

    @BeforeClass
    public static void initializeDB() throws Exception {
        ms = new ManagementSystem();
        ms.initializeDB();
    }

    @Test//(expected = IllegalArgumentException.class)
    public void testFindEpisode() throws Exception {
        assertEquals("There is not such record in database", new Episode("Winter is Coming", new Date(2011, 4, 17), 1l, 1l), ms.findEpisode(3l));
    }

    @Test
    public void testAddEpisode() throws Exception {
        Episode episode = new Episode("Ghost rider", new Date(2011, 4, 13), 1l, 2l);
        ms.addEpisode(episode, 2l);
        assertEquals("Add was failed", new Episode("Ghost rider", new Date(2011, 4, 13), 1l, 2l), ms.findEpisode(episode.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteEpisode() throws Exception {
        ms.deleteEpisode(1l);
        ms.findEpisode(1l);
    }

    @Test
    public void testChangeEpisode() throws Exception {
        Episode episode = ms.findEpisode(2l);
        episode.setTitle("Ali&NikitaProj");
        ms.changeEpisode(episode);
        assertEquals("Change was failed", new Episode("NikitaProj", new Date(2010, 5, 12), 1l, 0l), ms.findEpisode(2l));
    }

    @Test
    public void testAddShow() throws Exception {
        Show show = new Show("BBT", 2008l);
        ms.addShow(show);
        assertEquals("Add was failed", new Show("BBT", 2008l), ms.findShow(show.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteShow() throws Exception {
        ms.deleteShow(1l);
        ms.findShow(1l);
    }

    @Test
    public void testChangeShow() throws Exception {
        Show show = ms.findShow(2l);
        show.setName("Screppy doo");
        ms.changeShow(show);
        assertEquals("Change was failed", new Show("Screppy doo", 1965l), ms.findShow(2l));
    }

    @Test
    public void testFindShow() throws Exception {
        assertEquals("There is not such record in database", new Show("GOT", 2011l), ms.findShow(1l));
    }

    @Test
    public void testGetEpisodeList() throws Exception {

    }

    @Test
    public void testGetShowList() throws Exception {

    }
}
