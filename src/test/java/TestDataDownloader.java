import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.ncedu.showstool.DataDownloader;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Никита on 03.07.14.
 */
public class TestDataDownloader {
    @Mock
    DataDownloader mockedDownloader;

    @Before
    public void initialize_mock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFetchJSONByURL_anyCorrectURL_anyJSONString() {
        when(mockedDownloader.fetchJsonByUrl("anyCorrectURL")).thenReturn("anyJSONString");
        assertEquals("Not equals", "anyJSONString", mockedDownloader.fetchJsonByUrl("anyCorrectURL"));
        verify(mockedDownloader).fetchJsonByUrl("anyCorrectURL");
    }
}
